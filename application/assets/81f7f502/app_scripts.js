'use strict';


var testApp = angular.module('testApp', ['ngAnimate', 'ngRoute']);

//This configures the routes and associates each route with a view and a controller

testApp.config(function ($routeProvider, $locationProvider
    ) {

    $routeProvider.when('/', {
        templateUrl: '/assets/81f7f502/partials/index.html'
    });

    $routeProvider.when('/page1', {
        templateUrl: '/assets/81f7f502/partials/page1.html'
    });

    $routeProvider.when('/page2', {
        templateUrl: '/assets/81f7f502/partials/page2.html'
    });

    $routeProvider.otherwise({ redirectTo: '/' });
});



testApp.config(['$httpProvider', function($httpProvider) {
	$httpProvider.defaults.headers.common["FROM-ANGULAR"] = "true";
}]);

testApp.config(['$locationProvider', function($location) {
    $location.hashPrefix('!');
}]);

